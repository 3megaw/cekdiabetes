/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cekdiabet;
import java.sql.*;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Tri Mega W
 */
public class frmutama extends javax.swing.JFrame {
    
    Connection Con;
    ResultSet RsBrg;
    Statement stm;
    
    boolean ada=false;
    String sSatuan;
    boolean edit=false;
    
    private Object[][]dataTable=null;
    private String[] header={"ID","Glukosa","Tekanan Darah","BMI","Umur","Hasil"};
    
    /**
     * Creates new form 
     */
    public frmutama() {
        initComponents();
        open_db();
        baca_data();
        aktif(false);
        aktif2(false);
        setTombol(true);
        setTombol2(true);
        general();
    }
    ///KUMPULAN METHOD
    
    //method memindahkan data dari table ke form
    private void setField(){
    int row=tblBrg.getSelectedRow();
    txt_dataid.setText((String)tblBrg.getValueAt(row,0));
    txt_dataglukosa.setText((String)tblBrg.getValueAt(row,1));
    txt_datatekanandarah.setText((String)tblBrg.getValueAt(row,2));
    
    String data_bmi =Double.toString((Double)tblBrg.getValueAt(row,3)); 
    txt_databmi.setText(data_bmi);
    String data_umur =Integer.toString((Integer)tblBrg.getValueAt(row,4)); 
    txt_dataumur.setText(data_umur);
    String data_hasil =Integer.toString((Integer)tblBrg.getValueAt(row,5)); 
    txt_datahasil.setText(data_hasil);
    
    //cmbSatuan.setSelectedItem((String)tblBrg.getValueAt(row,2));
    //String harga = Double.toString((Double)tblBrg.getValueAt(row,3));
    //txtHarga.setText(harga); 
/*    String data_id=Integer.toString((Integer)tblBrg.getValueAt(row,1));
    txt_dataid.setText(data_id); 
    String data_glukosa =Integer.toString((Integer)tblBrg.getValueAt(row,2)); 
    txt_dataglukosa.setText(data_glukosa);
    String data_tekanandarah =Integer.toString((Integer)tblBrg.getValueAt(row,3)); 
    txt_datatekanandarah.setText(data_tekanandarah);
    String data_bmi =Integer.toString((Integer)tblBrg.getValueAt(row,4)); 
    txt_databmi.setText(data_bmi);
    */
    }
    
    
    
    //method membuka database server, user, pass
    private void open_db(){
        try{
                            Class.forName("com.mysql.jdbc.Driver");
            KoneksiMysql kon = new KoneksiMysql("localhost","root","","datamining");
            Con = kon.getConnection();
            System.out.println("Berhasil ");    
        }catch(Exception e){
            System.out.println("Error : "+e);
        }
    }
    
    
    
    //method baca data dari Mysql dimasukkan ke table pada form
    private void baca_data(){
        try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("select * from tugasbesar");
            
            ResultSetMetaData meta = RsBrg.getMetaData(); 
            int col = meta.getColumnCount();
            int baris = 0; 
             while(RsBrg.next()){
              baris = RsBrg.getRow();
             }
             
             dataTable = new Object[baris][col]; 
              int x = 0; 
              RsBrg.beforeFirst();
              while(RsBrg.next()){
                dataTable[x][0] = RsBrg.getString("No"); 
                dataTable[x][1] = RsBrg.getString("Glucose");
                dataTable[x][2] = RsBrg.getString("BloodPressure"); 
                dataTable[x][3] = RsBrg.getDouble("BMI"); 
                dataTable[x][4] = RsBrg.getInt("Age");                 
                dataTable[x][5] = RsBrg.getInt("Outcome");                 
                x++;   
              }
              tblBrg.setModel(new DefaultTableModel(dataTable,header)); 
        
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e); 
        }
    }
    
    
    
    //untuk mengkosongkan isian data 
    private void kosong(){
        txtid.setText("");     
        txtnama.setText("");     
        txtglukosa.setText("");     
        txttekanandarah.setText("");     
        txtbmi.setText("");
    }
    private void kosong2(){
        txt_dataid.setText("");     
        txt_databmi.setText("");     
        txt_dataglukosa.setText("");     
        txt_datatekanandarah.setText("");     
        txt_datahasil.setText("");
        txt_dataumur.setText("");
    }
    
    
    //mengset aktif tidak isian data 
    private void aktif(boolean x){
        txtid.setEnabled(x);     
        txtnama.setEnabled(x);
        txtumur.setEnabled(x);
        txtglukosa.setEnabled(x);     
        txttekanandarah.setEnabled(x);     
        txtbmi.setEnabled(x);     
    }
    private void aktif2(boolean x){
        txt_dataid.setEnabled(x);     
        txt_dataglukosa.setEnabled(x);
        txt_dataumur.setEnabled(x);
        txt_datatekanandarah.setEnabled(x);     
        txt_databmi.setEnabled(x);     
        txt_datahasil.setEnabled(x);     
    }
    
    
    
    //mengset tombol on/off 
    private void setTombol(boolean t){
         cmdTambah.setEnabled(t);    
         cmdSimpan.setEnabled(!t);    
         cmdKeluar.setEnabled(t);
    }
    
    private void setTombol2(boolean t){
        cmdBatal.setEnabled(!t);
        cmdsimpandata.setEnabled(!t);
        cmdubahdata.setEnabled(t);
        cmdhapusdata.setEnabled(t);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtid = new javax.swing.JTextField();
        txtnama = new javax.swing.JTextField();
        txtglukosa = new javax.swing.JTextField();
        txttekanandarah = new javax.swing.JTextField();
        txtbmi = new javax.swing.JTextField();
        cmdTambah = new javax.swing.JButton();
        cmdSimpan = new javax.swing.JButton();
        cmdBatal = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel9 = new javax.swing.JLabel();
        cmdReset = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        txt_dataglukosa = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_datatekanandarah = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txt_databmi = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txt_dataumur = new javax.swing.JTextField();
        cmdTambahData = new javax.swing.JButton();
        cmdsimpandata = new javax.swing.JButton();
        cmdubahdata = new javax.swing.JButton();
        cmdhapusdata = new javax.swing.JButton();
        jLabel15 = new javax.swing.JLabel();
        labelhasil = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txt_dataid = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_datahasil = new javax.swing.JTextField();
        txtumur = new javax.swing.JTextField();
        cek1 = new javax.swing.JLabel();
        cek2 = new javax.swing.JLabel();
        cek3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblBrg = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        cmdpasien = new javax.swing.JButton();
        cmdKeluar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Klasifikasi Pasien Penderita Diabetes");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 19, -1, -1));

        jLabel2.setText("Masukan data pasien :");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 93, -1, -1));

        jLabel3.setText("ID");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 118, -1, -1));

        jLabel4.setText("Nama");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 169, -1, -1));

        jLabel5.setText("Glukosa");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 224, -1, 21));

        jLabel6.setText("Tekanan Darah");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 282, -1, -1));

        jLabel7.setText("BMI");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 338, -1, -1));
        getContentPane().add(txtid, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 138, 246, -1));

        txtnama.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnamaActionPerformed(evt);
            }
        });
        getContentPane().add(txtnama, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 193, 246, -1));

        txtglukosa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtglukosaActionPerformed(evt);
            }
        });
        getContentPane().add(txtglukosa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 251, 246, -1));
        getContentPane().add(txttekanandarah, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 307, 246, -1));
        getContentPane().add(txtbmi, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 358, 246, -1));

        cmdTambah.setText("Tambah");
        cmdTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdTambahActionPerformed(evt);
            }
        });
        getContentPane().add(cmdTambah, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 470, 246, -1));

        cmdSimpan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cmdSimpan.setText("Cek");
        cmdSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdSimpanActionPerformed(evt);
            }
        });
        getContentPane().add(cmdSimpan, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 510, 113, -1));

        cmdBatal.setText("Batal");
        cmdBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdBatalActionPerformed(evt);
            }
        });
        getContentPane().add(cmdBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 570, 130, -1));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Algoritma Naive Bayes");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 42, -1, -1));
        getContentPane().add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 77, 246, 10));

        jLabel9.setText("Umur");
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 389, -1, -1));

        cmdReset.setText("Reset");
        cmdReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdResetActionPerformed(evt);
            }
        });
        getContentPane().add(cmdReset, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 540, 113, -1));

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(282, 103, -1, -1));

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Glukosa");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(391, 461, -1, -1));
        getContentPane().add(txt_dataglukosa, new org.netbeans.lib.awtextra.AbsoluteConstraints(391, 481, 197, -1));

        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Tekanan Darah");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(391, 512, -1, -1));
        getContentPane().add(txt_datatekanandarah, new org.netbeans.lib.awtextra.AbsoluteConstraints(391, 532, 197, -1));

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("BMI");
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 404, -1, -1));
        getContentPane().add(txt_databmi, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 429, 173, -1));

        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Umur");
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 460, -1, -1));
        getContentPane().add(txt_dataumur, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 480, 173, -1));

        cmdTambahData.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cmdTambahData.setText("Tambah Data");
        cmdTambahData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdTambahDataActionPerformed(evt);
            }
        });
        getContentPane().add(cmdTambahData, new org.netbeans.lib.awtextra.AbsoluteConstraints(855, 404, 142, 57));

        cmdsimpandata.setText("Simpan Data");
        cmdsimpandata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdsimpandataActionPerformed(evt);
            }
        });
        getContentPane().add(cmdsimpandata, new org.netbeans.lib.awtextra.AbsoluteConstraints(855, 479, 142, -1));

        cmdubahdata.setText("Ubah Data");
        cmdubahdata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdubahdataActionPerformed(evt);
            }
        });
        getContentPane().add(cmdubahdata, new org.netbeans.lib.awtextra.AbsoluteConstraints(855, 520, 142, -1));

        cmdhapusdata.setText("Hapus Data");
        cmdhapusdata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdhapusdataActionPerformed(evt);
            }
        });
        getContentPane().add(cmdhapusdata, new org.netbeans.lib.awtextra.AbsoluteConstraints(855, 561, 142, -1));

        jLabel15.setText("Hasil :");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 510, -1, -1));

        labelhasil.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        getContentPane().add(labelhasil, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 530, 135, 37));

        jLabel17.setForeground(new java.awt.Color(255, 255, 255));
        jLabel17.setText("ID");
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(391, 404, -1, -1));

        txt_dataid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_dataidActionPerformed(evt);
            }
        });
        getContentPane().add(txt_dataid, new org.netbeans.lib.awtextra.AbsoluteConstraints(391, 429, 197, -1));

        jLabel18.setForeground(new java.awt.Color(255, 255, 255));
        jLabel18.setText("Hasil");
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 511, -1, -1));

        txt_datahasil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_datahasilActionPerformed(evt);
            }
        });
        getContentPane().add(txt_datahasil, new org.netbeans.lib.awtextra.AbsoluteConstraints(615, 531, 173, -1));
        getContentPane().add(txtumur, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 409, 246, -1));
        getContentPane().add(cek1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 435, -1, -1));
        getContentPane().add(cek2, new org.netbeans.lib.awtextra.AbsoluteConstraints(88, 436, -1, -1));
        getContentPane().add(cek3, new org.netbeans.lib.awtextra.AbsoluteConstraints(147, 448, -1, -1));

        jPanel1.setBackground(new java.awt.Color(0, 153, 153));

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 255, 255));
        jLabel10.setText("Dataset :");

        tblBrg.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Glukosa", "Tekanan Darah", "BMI", "Umur", "Hasil"
            }
        ));
        tblBrg.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblBrgMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblBrg);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 709, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(230, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(282, 70, 740, 550));

        jPanel3.setBackground(new java.awt.Color(0, 102, 102));

        cmdpasien.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cmdpasien.setText("Data Pasien");
        cmdpasien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdpasienActionPerformed(evt);
            }
        });

        cmdKeluar.setText("Keluar");
        cmdKeluar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdKeluarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmdpasien, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 533, Short.MAX_VALUE)
                .addComponent(cmdKeluar)
                .addGap(28, 28, 28))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmdpasien)
                    .addComponent(cmdKeluar))
                .addContainerGap(575, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 0, 750, 620));
        getContentPane().add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 250, 10));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdKeluarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdKeluarActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_cmdKeluarActionPerformed

    
    int hasil_countnol;
    int hasil_countsatu;
    int hasil_counttotal;
    public float pc1;
    public float pc2;
    float ppc1,ppc2;
    private void cmdSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdSimpanActionPerformed
        // TODO add your handling code here:
        String tid=txtid.getText();     
        String tNama=txtnama.getText();
        float tbmi=Float.parseFloat(txtbmi.getText());
        float tglukosa=Float.parseFloat(txtglukosa.getText());
        int tdarah=Integer.parseInt(txttekanandarah.getText());     
        int tumur=Integer.parseInt(txtumur.getText());
        
        general();
       
        pc1=(float)hasil_countnol/hasil_counttotal; //PROB C1
        pc2=(float)hasil_countsatu/hasil_counttotal; //PROB C2
        
        //ALGO GLUKOSA//
        int c_glukosa0 = 0;
        int c_glukosa1 = 0;
        if(tglukosa<=100){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Glucose>=100 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_glukosa0= f;
            }
            catch(SQLException e){
            System.out.println("Query glukosa salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Glucose>=100 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_glukosa1= f;
            }
            catch(SQLException e){
            System.out.println("Query glukosa salah");
            }
        }else if(tglukosa>100 && tglukosa<=125){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Glucose BETWEEN 101 AND 125 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_glukosa0= f;
            }
            catch(SQLException e){
            System.out.println("Query glukosa salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Glucose BETWEEN 101 AND 125 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_glukosa1= f;
            }
            catch(SQLException e){
            System.out.println("Query glukosa salah");
            }
        }else{
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Glucose >125 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_glukosa0= f;
            }
            catch(SQLException e){
            System.out.println("Query glukosa salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Glucose >125 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_glukosa1= f;
            }
            catch(SQLException e){
            System.out.println("Query glukosa salah");
            }
        }
        
        //ALGO TEKANAN DARAH//
        int c_tekanandarah0=0;
        int c_tekanandarah1=0;
        if(tdarah<80){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure <80 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah0= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure <80 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah1= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
        }else if(tdarah==80){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure =80 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah0= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure =80 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah1= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
        }else if(tdarah>80 && tdarah<=90){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure BETWEEN 81 AND 90 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah0= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure BETWEEN 81 AND 90 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah1= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
        }else if(tdarah>91 && tdarah<=120){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure BETWEEN 91 AND 120 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah0= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure BETWEEN 91 AND 120 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah1= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
        }else{
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure >120 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah0= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BloodPressure >120 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_tekanandarah1= f;
            }
            catch(SQLException e){
            System.out.println("Query tekanan darah salah");
            }
        }
        
        //ALGO BMI
        int c_bmi0=0;
        int c_bmi1=0;
        if(tbmi>=18.5 && tbmi<25){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI BETWEEN 17 AND 24.9 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi0= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI BETWEEN 17 AND 24.9 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi1= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
        }else if(tbmi>=25 && tbmi<30){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI BETWEEN 25 AND 29.9 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi0= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI BETWEEN 25 AND 29.9 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi1= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
        }else if(tbmi>=30 && tbmi<35){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI BETWEEN 30 AND 34.9 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi0= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI BETWEEN 30 AND 34.9 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi1= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
        }else if(tbmi>=35 && tbmi<40){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI BETWEEN 35 AND 39.9 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi0= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI BETWEEN 35 AND 39.9 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi1= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
        }else{
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI >40 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi0= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE BMI >40 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_bmi1= f;
            }
            catch(SQLException e){
            System.out.println("Query bmi salah"); 
            }
        }
        
        //ALGO UMUR//
        int c_umur0=0;
        int c_umur1=0;
        if(tumur>=1 && tumur<=35){
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Age BETWEEN 0 AND 35 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_umur0= f;
            }
            catch(SQLException e){
            System.out.println("Query umur salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Age BETWEEN 0 AND 35 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_umur1= f;
            }
            catch(SQLException e){
            System.out.println("Query umur salah");
            }
        }else{
        try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Age BETWEEN 36 AND 70 AND Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_umur0= f;
            }
            catch(SQLException e){
            System.out.println("Query umur salah");
            }
            try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Age BETWEEN 36 AND 70 AND Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_umur1= f;
            }
            catch(SQLException e){
            System.out.println("Query umur salah");
            }
        }
        int c_outcome0=0;
        int c_outcome1=0;
        try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_outcome0= f;
            }
            catch(SQLException e){
            System.out.println("Query outcome salah");
            }
        try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Outcome=1");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            c_outcome1= f;
            }
            catch(SQLException e){
            System.out.println("Query outcome salah");
            }
        
        float ans0=0;
        float ans1=0;
        ans0 = (float)c_glukosa0 / (float)c_outcome0  * (float)c_tekanandarah0 /(float)c_outcome0 * (float)c_bmi0 /(float)c_outcome0 * (float)c_umur0 /(float)c_outcome0;
        ans1 = (float)c_glukosa1 / (float)c_outcome1  * (float)c_tekanandarah1 /(float)c_outcome1 * (float)c_bmi1 /(float)c_outcome1 * (float)c_umur1 /(float)c_outcome1;
        
          //  cek1.setText(Float.toString(ans0));
          //  cek2.setText(Float.toString(ans1));
        
        
        //ppc1 = cariprob(tglukosa,tdarah,tbmi,tumur,1);
        //ppc2 = cariprob(tglukosa,tdarah,tbmi,tumur,0);

        ans0 = ans0 * pc1;
        ans1 = ans1 * pc2;
        
      //  cek1.setText(""+pc1);
        //cek2.setText(""+pc2);
        //cek3;
        
        if(ans0<ans1){
            labelhasil.setText("POSITIF");
        }else{
            labelhasil.setText("NEGATIF");
        }
        //int valuehasil = Integer.parseInt(labelhasil.getText());
        String hasilbelumif = labelhasil.getText().toString();
        
        int valuehasil=0;
        if (hasilbelumif=="POSITIF"){
            valuehasil=1;
        }else if(hasilbelumif=="NEGATIF"){
            valuehasil=0;
        }
        
        String hasilgan = labelhasil.getText().toString();
        
        try{
           // if (edit==true){
            //    stm.executeUpdate("update pasien set nama='"+tNama+"',glukosa='"+tglukosa+"',bmi="+tbmi+",tekanandarah="+tdarah+" where id_pasien='" + tid + "'"); 
           // }else{
                 stm.executeUpdate("INSERT into pasien (id_pasien,nama,glukosa,tekanandarah,bmi,umur,hasil) VALUES('"+tid+"','"+tNama+"','"+tglukosa+"','"+tdarah+"','"+tbmi+"','"+tumur+"','"+valuehasil+"')");
           // }
           //  tblBrg.setModel(new DefaultTableModel(dataTable,header)); 
           //  baca_data();         
             aktif(false);         
             setTombol(true);
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_cmdSimpanActionPerformed

    private void cmdTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdTambahActionPerformed
        // TODO add your handling code here:
         aktif(true); 
         setTombol(false); 
          kosong(); 
    }//GEN-LAST:event_cmdTambahActionPerformed

    private void cmdBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdBatalActionPerformed
        // TODO add your handling code here:
        aktif2(false);  
        setTombol2(false);
    }//GEN-LAST:event_cmdBatalActionPerformed

    private void tblBrgMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblBrgMouseClicked
        // TODO add your handling code here:
        setField();
    }//GEN-LAST:event_tblBrgMouseClicked

    private void txtnamaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnamaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnamaActionPerformed

    private void txtglukosaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtglukosaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtglukosaActionPerformed

    private void cmdResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdResetActionPerformed
        // TODO add your handling code here:
        kosong();
    }//GEN-LAST:event_cmdResetActionPerformed

    private void txt_datahasilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_datahasilActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_datahasilActionPerformed

    private void cmdTambahDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdTambahDataActionPerformed
        // TODO add your handling code here:
        aktif2(true);
        setTombol2(false);
        kosong2();
    }//GEN-LAST:event_cmdTambahDataActionPerformed

    private void txt_dataidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_dataidActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txt_dataidActionPerformed

    private void cmdsimpandataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdsimpandataActionPerformed
        // TODO add your handling code here: SIMPAN DATA
        int did=Integer.parseInt(txt_dataid.getText());     
        float dglukosa=Float.parseFloat(txt_dataglukosa.getText());
        int dtekanandarah=Integer.parseInt(txt_datatekanandarah.getText());
        float dbmi=Float.parseFloat(txt_databmi.getText());
        int dumur=Integer.parseInt(txt_dataumur.getText());
        int dhasil=Integer.parseInt(txt_datahasil.getText());
        
        try{
            if (edit==true){
                stm.executeUpdate("UPDATE tugasbesar set Glucose='"+dglukosa+"',BMI='"+dbmi+"',BloodPressure='"+dtekanandarah+"',Age='"+dumur+"',Outcome='"+dhasil+"' where No='" + did + "'"); 
            }else{
                 stm.executeUpdate("INSERT into tugasbesar (No,Glucose,BloodPressure,BMI,Age,Outcome) VALUES('"+did+"','"+dglukosa+"','"+dtekanandarah+"','"+dbmi+"','"+dumur+"','"+dhasil+"')");
            }
             tblBrg.setModel(new DefaultTableModel(dataTable,header)); 
           //  baca_data();         
             aktif2(false);   
             baca_data();
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e);
        }
    }//GEN-LAST:event_cmdsimpandataActionPerformed

    private void cmdhapusdataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdhapusdataActionPerformed
        // TODO add your handling code here:
        try{     
            String sql="delete from tugasbesar where No='" + txt_dataid.getText() + "'";     
            stm.executeUpdate(sql);     
            baca_data();     
        }catch(SQLException e)     
        {         
            JOptionPane.showMessageDialog(null, e);     
        }
    }//GEN-LAST:event_cmdhapusdataActionPerformed

    private void cmdubahdataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdubahdataActionPerformed
        // TODO add your handling code here:
        edit=true;
        aktif2(true);     
        txt_dataid.setEditable(false);
    }//GEN-LAST:event_cmdubahdataActionPerformed

    private void cmdpasienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdpasienActionPerformed
        // TODO add your handling code here:
        pasien pasienmenu = new pasien();
        pasienmenu.setVisible(true);
    }//GEN-LAST:event_cmdpasienActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmutama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmutama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmutama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmutama.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmutama().setVisible(true);
            }
        });
    }
    
    //MULAI NAIVE BAYESNYA POKOKNYA 
    
    private void general(){
    try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Outcome=0");
            RsBrg.next();
            int f = RsBrg.getInt(1);
            hasil_countnol= f;
//            labelhasil.setText(""+f);
//            System.out.println("Number of records in the cricketers_data table: "+f);
        }
        catch(SQLException e){
        System.out.println("Error TXT");
        }
    
    try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar` WHERE Outcome=1");
            RsBrg.next();
            int ff = RsBrg.getInt(1);
            hasil_countsatu= ff;
//            labelhasil.setText(""+ff);
//            System.out.println("Number of records in the cricketers_data table: "+f);
        }
        catch(SQLException e){
        System.out.println("Error TXT");
        }
    
    try{
            stm = Con.createStatement();
            RsBrg = stm.executeQuery("SELECT COUNT(*) FROM `tugasbesar`");
            RsBrg.next();
            int fff = RsBrg.getInt(1);
            hasil_counttotal= fff;
//            labelhasil.setText(""+fff);
//            System.out.println("Number of records in the cricketers_data table: "+f);
        }
        catch(SQLException e){
        System.out.println("Error TXT");
        }
    
        
    
    // cek1.setText(""+hasil_countnol);
     //cek2.setText(""+hasil_countsatu);
    // cek3.setText(""+pc2);
       
    
    }
    
    public float cariprob(float glukosa, int tekanan, float bmi, int umur, int class1){
        float ans=0;
        
        try {
		Connection con = DriverManager.getConnection("jdbc:odbc:datamining");
		Statement s = con.createStatement();
		String query = null;
		ResultSet rs = null;
                int a=0 , b=0 , c=0 , d=0 ,e=0,f=0, total=0;
                
               
                query ="SELECT COUNT(*) FROM tugasbesar WHERE (Glucose >=126 ) AND (Outcome = '" +class1 +"') ";
			s.execute(query);
			rs= s.getResultSet();
			if(rs.next())
			//a=Integer.parseInt(rs.getString(1));
                        a=rs.getInt(1);
                        
                query ="SELECT COUNT(*) FROM tugasbesar WHERE (BloodPressure BETWEEN 90 AND 140 ) AND (Outcome = '" +class1 +"') ";
			s.execute(query);
			rs= s.getResultSet();
			if(rs.next())
			//b=Integer.parseInt(rs.getString(1));
                        b=rs.getInt(1);
                        
                query ="SELECT COUNT(*) FROM tugasbesar WHERE (BMI BETWEEN 30 AND 50 ) AND (Outcome = '" +class1 +"') ";
			s.execute(query);
			rs= s.getResultSet();
			if(rs.next())
			//c=Integer.parseInt(rs.getString(1));
                        c=rs.getInt(1);
                        
                query ="SELECT COUNT(*) FROM tugasbesar WHERE (Age BETWEEN 35 AND 60 ) AND (Outcome = '" +class1 +"') ";
			s.execute(query);
			rs= s.getResultSet();
			if(rs.next())
			//d=Integer.parseInt(rs.getString(1));
                        d=rs.getInt(1);
                        
                query ="SELECT COUNT(*) FROM tugasbesar WHERE (Outcome = '"+ class1 + "' )";
			s.execute(query);
			rs= s.getResultSet();
			if(rs.next())
			//total=Integer.parseInt(rs.getString(1));
                        total=rs.getInt(1);
     
                ans = (float)a / (float)total * (float)b /(float)total * (float)c /(float)total * (float)d /(float)total ;
			//calculating total probability by naive bayes

                       // cek1.setText(String.valueOf(a));
                       // cek3.setText(String.valueOf(c));                        
                        
		s.close();
		con.close();
                
                
//                cek3.setText(Integer.toString(total));                        
                
        } catch (SQLException ex) {
            System.out.println("probnya salah");
            
        }
            
        
        return ans;
    }
    
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel cek1;
    private javax.swing.JLabel cek2;
    private javax.swing.JLabel cek3;
    private javax.swing.JButton cmdBatal;
    private javax.swing.JButton cmdKeluar;
    private javax.swing.JButton cmdReset;
    private javax.swing.JButton cmdSimpan;
    private javax.swing.JButton cmdTambah;
    private javax.swing.JButton cmdTambahData;
    private javax.swing.JButton cmdhapusdata;
    private javax.swing.JButton cmdpasien;
    private javax.swing.JButton cmdsimpandata;
    private javax.swing.JButton cmdubahdata;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel labelhasil;
    private javax.swing.JTable tblBrg;
    private javax.swing.JTextField txt_databmi;
    private javax.swing.JTextField txt_dataglukosa;
    private javax.swing.JTextField txt_datahasil;
    private javax.swing.JTextField txt_dataid;
    private javax.swing.JTextField txt_datatekanandarah;
    private javax.swing.JTextField txt_dataumur;
    private javax.swing.JTextField txtbmi;
    private javax.swing.JTextField txtglukosa;
    private javax.swing.JTextField txtid;
    private javax.swing.JTextField txtnama;
    private javax.swing.JTextField txttekanandarah;
    private javax.swing.JTextField txtumur;
    // End of variables declaration//GEN-END:variables
}
